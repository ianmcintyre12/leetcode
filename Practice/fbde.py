# Practice Take two positive numbers as strings, and return the sum of them.  E.g. "3.14" + "0.9" => "4.04".



# Please note: Simply converting the strings to numbers and adding them together or utilizing Big Decimal is not acceptable and will not get full credit for the assessment.  The solution must work for numbers that are very large as well.


# def add(s1,s2):
#     lower = s1.split('.')
#     higher = s2.split('.')

#     if len(lower[1]) > len(higher[1]):
#         higher,lower = lower,higher
#     while len(higher[1]) != len(lower[1]):
#         lower[1] += '0'

#     dec_reverse = ''
#     carry = 0
#     pointer = -1
#     while -pointer != (len(higher[1])) + 1:
#         first = int(higher[1][pointer])
#         second = int(lower[1][pointer])
#         tot = first + second
#         if carry > 0:
#             tot += 1
#             carry -= 1
#         if tot > 9:
#             tot -=10
#             carry += 1
#         dec_reverse += str(tot)
#         pointer -= 1
#     dec = dec_reverse[::-1]
#     whole_reverse = ''
#     pointer=-1
#     while -pointer != (len(higher[0]) +1 or -pointer != (len(lower[0])) + 1):
#         first = 0
#         second = 0
#         if -pointer !=(len(higher[0])) +1:
#             first = int(higher[0][pointer])
#         if -pointer != (len(lower[0]))+1:
#             second=int(lower[0][pointer])
#         tot = first + second
#         if carry:
#             tot = tot + carry
#             carry = 0
#         if tot > 9:
#             carry += 1
#             tot -= 10
#         whole_reverse += str(tot)
#         pointer -= 1

#     if carry:
#         whole_reverse += str(carry)

#     whole = whole_reverse[::-1]
#     return whole +'.'+dec


def add(s1,s2):
    int1 = s1.split('.')
    int2 = s2.split('.')

    def equalize(num1,num2):
        if len(num1) == len(num2):
            return [num1,num2]
        lower = num1
        higher = num2
        if len(num1) > len(num2):
            lower = num2
            higher = num1

        while len(higher) != len(lower):
            lower += '0'
        return [higher,lower]

    def add_equalized(nums,carry):
        reverse_num = ''
        pointer = -1
        carry = carry
        num1 = nums[0]
        num2 = nums[1]
        while - pointer != len(num1) + 1:
            first = int(num1[pointer])
            second = int(num2[pointer])
            tot = first + second
            if carry > 0:
                tot += carry
                carry -=1
            while tot > 9:
                carry += 1
                tot -=10
            reverse_num += str(tot)
            pointer -= 1
        return [reverse_num[::-1],carry]

    def add_equalized_whole(nums,carry):
        reverse_num = ''
        pointer = -1
        carry = carry
        num1 = nums[0][::-1]
        num2 = nums[1][::-1]
        while -pointer != len(num1) + 1:
            first = int(num1[pointer])
            second = int(num2[pointer])
            tot = first + second
            while carry > 0:
                tot += 1
                carry -= 1
            while tot > 9:
                carry +=1
                tot -= 10
            if -pointer == len(num1) and carry:
                tot = str(tot + carry*10)
                tot = tot[::-1]
                reverse_num += tot
            else:
                reverse_num += str(tot)
            pointer -=1

        return reverse_num[::-1]

    decimal_equalize = equalize(int1[1],int2[1])
    decimal_add = add_equalized(decimal_equalize,0) #1.063

    whole_equalize = equalize(int1[0][::-1],int2[0][::-1])
    whole_add = add_equalized_whole(whole_equalize,decimal_add[1])
    return whole_add +'.'+decimal_add[0]




s1='3.14'
s2='0.9'
print(add(s1,s2))
