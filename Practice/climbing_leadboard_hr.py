def climbingLeaderboard(ranked, player):
    # Write your code here
    dupesout = sorted((set(ranked)))
    ranks = []
    pointer_ranked = 0
    pointer_player = 0
    while pointer_player < len(player):
        if dupesout[0] > player[pointer_player]:
            dupesout = [player[pointer_player]] + dupesout
            pointer_player += 1
            ranks.append(len(dupesout))
        elif dupesout[pointer_ranked] > player[pointer_player]:
            if dupesout[pointer_ranked] == player[pointer_player]:
                ranks.append(len(dupesout) - pointer_ranked)
                pointer_player += 1
                continue
            dupesout.insert(pointer_ranked, player[pointer_player])
            ranks.append(len(dupesout) - pointer_ranked)
            pointer_player += 1
        elif pointer_ranked == len(dupesout) - 1:
            dupesout.append(player[pointer_player])
            pointer_player += 1
            ranks.append(1)
        pointer_ranked += 1

    return ranks






ranked = [100, 90, 90, 80, 75, 60]
player = [50, 65, 77,77, 90, 102]
print(climbingLeaderboard(ranked,player))
