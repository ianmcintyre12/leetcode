function longestConsec(strarr, k) {
    // your code
  if (strarr.length < k || strarr.length === 0 || k <= 0){
    return ''
  }

  function comparelength(a,b){
    return b[0].length - a[0].length
  }
  let index_arr = strarr.map((x) =>[x,strarr.indexOf(x)])

  index_arr.sort(comparelength)


  let longest = index_arr.slice(0,k)

  function compareindex(a,b){
    return a[1] - b[1]
  }

  longest.sort(compareindex)
  console.log(longest)

  let ans = ''
  for (let word of longest){
    ans += word[0]
  }
  return ans
}

function longestConsec(strarr, k) {
    // your code
  if (strarr.length < k || strarr.length === 0 || k <= 0){
    return ''
  }
  let longest = ''

  for(let i =0; i<strarr.length - k + 1; i++){
    let slice = strarr.slice(i,i+k)
    let word = slice.join('')
    if (word.length > longest.length){
      longest = word
    }
  }
  return longest
}

console.log(longestConsec(["zone", "abigail", "theta", "form", "libe", "zas"], 2))
console.log(Math.max(1,2))
