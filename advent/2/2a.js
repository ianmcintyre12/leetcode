const input = require('fs')

const data1 = input.readFile('advent/2/2a.txt', 'utf8',  (err,d) => {
    if(err){
        console.error(err)
        return
    }
    let data = d.split('\r\n')
    data = data.slice(0,data.length - 1)
    // console.log(data)

    function countMax(games){
        let total = 0

        for(let game of games){
            let counts = {
                'red': 0,
                'blue': 0,
                'green':0,
            }


            items = game.split(/; |: |;/)
            g = items.slice(1,items.length)
            // console.log(g)
            for(let run of g){
                g_count = run.split(', ')
                // console.log('run:',g_count)
                for(let count of g_count){
                    imp = count.split(' ')
                    // console.log('count:',imp)
                    counts[imp[1]] = Math.max(imp[0], counts[imp[1]])
                }
            }
            if(counts['red']<13 && counts['blue']<15 && counts['green'] < 14){
                console.log(counts)
                let id = items[0].split(' ')
                total += parseInt(id[1])
            }
        }
        return total
    }

    console.log(countMax(data))
})
