const input = require('fs')

const data1 = input.readFile('advent/test.txt', 'utf8',  (err,d) => {
    if(err){
        console.error(err)
        return
    }
    let data = d.split('\r\n')
    let removed = []
    for(let d of data){
        if(d !== ''){
            removed.push(d)
        }
    }
    let kv = {}
    for(let d of removed){
        s = d.split(' ')
        kv[s[0]] = s[1]
    }
    let max = Object.keys(kv).reduce((a,b)=> Math.max(a,b),-Infinity)

    let result = ''

    let i = 1
    let inc = 1
    while(i<= max){
        result += kv[i]
        result += ' '
        inc += 1
        i += inc
    }
    return result
})
