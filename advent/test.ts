interface person {
    name: string;
    age: number;
    hasACar: boolean;
}

function youngest_owner(owners:person[]):string{
    let youngest:person = {'age': Infinity, 'name':'MegaOldplaceholder', 'hasACar':true}
    for(let owner of owners){
        if(owner.hasACar && owner.age < youngest.age ){
            youngest = owner
        }
    }
    return youngest.name
}



const cars = [
    {'name':'me','age':1,'hasACar':true},
    {'name':'m1','age':4,'hasACar':true},
    {'name':'m2','age':0,'hasACar':false},
    {'name':'m3','age':5,'hasACar':true}
]


console.log(youngest_owner(cars))
