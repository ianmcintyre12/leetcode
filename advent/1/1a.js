const input = require('fs')

const data1 = input.readFile('advent/test.txt', 'utf8',  (err,d) => {
    if(err){
        console.error(err)
        return 11
    }

    function findInt(item,start,increment_int){
        if (item.length === 0){
            return 0
        }
        let pointer = start
        while(isNaN(item[pointer])){
            console.log(item[pointer])
            pointer += increment_int
        }
        console.log('stop')
        return item[pointer]
    }

    let data = d.split('\r\n')
    let code = 0
    for(let item of data){
        const first = findInt(item,0,1)
        const second = findInt(item,item.length - 1,-1)
        code += parseInt(first + second)
    }
    console.log(code)

})
