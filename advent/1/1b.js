const input = require('fs')

const data1 = input.readFile('advent/Advent.txt', 'utf8',  (err,d) => {
    if(err){
        console.error(err)
        return 11
    }

    function findInt(item){
        if (item.length === 0){
            return 0
        }
        let norm = {
            '1': 'one', '2':'two','3':'three',
            '4': 'four', '5':'five','6':'six',
            '7': 'seven', '8':'eight','9':'nine',
        }
        let parsed = ''
        for(let char of item){
            if(char in norm){
                parsed += norm[char]
            } else{
                parsed += char
            }
        }

        first = [parsed.length, 'NaN']
        for( let number in norm){
            let index = parsed.indexOf(norm[number])
            if(index < first[0] && index > -1){
                first = [index,number]
            }
        }
        last = [-1, 'NaN']
        for( let number in norm){
            let index = parsed.lastIndexOf(norm[number])
            if(index > last[0] && index > -1){
                last = [index,number]
            }
        }
        return first[1]+last[1]
    }
    // console.log(findInt('zoneight234'))
    let data = d.split('\r\n')
    let code = 0

    for(let item of data){
        code += parseInt(findInt(item))
    }
    console.log(code)

    // }
})
