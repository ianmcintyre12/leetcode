const input = require('fs')
const { type } = require('os')

const data1 = input.readFile('advent/3/3a.txt', 'utf8',  (err,d) => {
    if(err){
        console.error(err)
        return
    }
    let data = d.split('\r\n')
    // console.log(data)

    function checkNear(data,sym_pos,line_pos){
        let finds = []
        let arr = [
            [data[line_pos -1][sym_pos -3],data[line_pos -1][sym_pos -2],data[line_pos -1][sym_pos -1],data[line_pos -1][sym_pos],data[line_pos -1][sym_pos +1],data[line_pos -1][sym_pos +2],data[line_pos -1][sym_pos +3]],
            [data[line_pos ][sym_pos -3],  data[line_pos][sym_pos -2],   data[line_pos][sym_pos -1],   data[line_pos][sym_pos],   data[line_pos][sym_pos + 1],  data[line_pos][sym_pos +2],   data[line_pos][sym_pos +3]],
            [data[line_pos +1][sym_pos -3],data[line_pos +1][sym_pos -2],data[line_pos+1][sym_pos-1],  data[line_pos+1][sym_pos], data[line_pos+1][sym_pos+1],  data[line_pos +1][sym_pos +2],data[line_pos +1][sym_pos +3]],
                ]
        for (let x of arr){
            let satisfy= false
            let check = x.slice(2,5)
            for(let y of check){
                if(!isNaN(y)){
                    satisfy = true
                    break
                }
            }
            if(satisfy){
                let p1 = 2
                let p2 = 2
                while(p1< check.length + 2){
                    if(!isNaN(check[p1-2])){
                        while(!isNaN(x[p1 - 1])){
                            p1 -= 1
                        }
                        while(!isNaN(x[p2 +1])){
                            p2 += 1
                        }
                        let num = ''
                        for(n of x.slice(p1,p2+1)){
                            num += n
                        }
                        finds.push(parseInt(num))
                    }
                    p2 += 1
                    p1 = p2
                    }
                }
            }
            if(finds.length == 2){
                return [finds[0] * finds[1]]
            }
            return [0]
        }
        // console.log(arr)

    let parts = []
    for(let line of data){
        let start = 0
        // let end = 0
        while(start < line.length){
            if(isNaN(line[start]) && line[start] != '.'){
                    let line_pos = data.indexOf(line)
                    let conf = checkNear(data,start,line_pos)
                    parts = [...parts,...conf]
                    // if start - 1 ,end, indexof(line) strart -> end +-1,
                    // start = end
            }
            start += 1
        }
    }
    console.log(parts)
    function add(accumulator, a) {
        return accumulator + a;
    }
    const sum = parts.reduce(add, 0);
    console.log(sum)
})
