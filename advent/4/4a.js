const input = require('fs')

const data1 = input.readFile('advent/4/4a.txt', 'utf8',  (err,d) => {
    if(err){
        console.error(err)
        return
    }
    let data = d.split('\r\n')
    data = data.slice(0,data.length - 1)
    // console.log(data)

    let total = 0
    for(let card of data){
        let sep = card.split(/: | \| /)
        let card_num = sep[0].split(/ |  /)
        let win_num = sep[1].split(/ |  /)
        let ur_num = sep[2].split(/ |  /)
        console.log(card_num)
        console.log(win_num)
        console.log(ur_num)

        let count = 0

        for(let num of win_num){
            if (num == '' || num == ' '){
                continue
            }
            if(ur_num.includes(num)){
                count == 0 ? count = 1 : count *= 2
            }
        }
        total += count
    }
    console.log(total)
})
