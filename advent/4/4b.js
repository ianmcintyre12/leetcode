const input = require('fs')

const data1 = input.readFile('advent/4/4a.txt', 'utf8',  (err,d) => {
    if(err){
        console.error(err)
        return
    }
    let data = d.split('\r\n')
    data = data.slice(0,data.length - 1)
    // console.log(data)

    let split_cards = []
    for(let card of data){
        let sep = card.split(/: | \| /)
        let card_num = sep[0].split(' ')
        while (card_num.indexOf('') > -1){
            card_num.splice(card_num.indexOf(''),1)
        }
        card_num.push(1)
        let win_num = sep[1].split(/ |  /)
        let ur_num = sep[2].split(/ |  /)
        // console.log(card_num)
        // console.log(win_num)
        // console.log(ur_num)
        split_cards.push([card_num,win_num,ur_num])
    }

    for(let card of split_cards){
        let count = 0

        for(let num of card[1]){
            if (num == '' || num == ' '){
                continue
            }
            if(card[2].includes(num)){
                count += 1
            }
        }
        // console.log(count)
        cur_card = split_cards.indexOf(card)
        // console.log('index',cur_card)
        while(count != 0){
            cur_card += 1
            if(cur_card == split_cards.length){
                break
            }
            split_cards[cur_card][0][2] += split_cards[split_cards.indexOf(card)][0][2]

            count -= 1
        }
        // console.log(split_cards)
    }
    function add(accumulator, a) {
        return accumulator + a[0][2];
    }
    const sum = split_cards.reduce(add, 0);
    console.log(sum)
})
