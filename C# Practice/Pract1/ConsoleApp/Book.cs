using System.Formats.Asn1;

class Book
{
    private string title;
    private string author;
    private int pages;
    public static int bookcount = 0;
    public Book(string aTitle, string aAuthor, int aPages)
    {
        Title = aTitle;
        author = aAuthor;
        pages =aPages;
        bookcount++;
    }

    public string Title {
        get{ return title;}
        set
        {
            if(value =="mebook")
            {
                title = value;
            }
            else
            {
                title = "naw";
            }
        }
    }
    public static void StaticTest(){
        Console.WriteLine("i can use this now");
    }

    public virtual void Type(){
        Console.WriteLine("book me");
    }
}
