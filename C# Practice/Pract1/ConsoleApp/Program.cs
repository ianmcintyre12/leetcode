﻿internal class Program
{
    private static void Main(string[] args)
    {
        Book book1 = new Book("meboffok","meguy",200);
        Console.WriteLine(book1.Title);
        Book.StaticTest();
        BigBook book2 = new BigBook("meep","you",200,"big");
        book2.Type();
/*
        // string characterName = "pooper";
        // char onlyonecharacter = 'a';
//         Float - 7 digits (32 bit)
//         Double-15-16 digits (64 bit)
//         Decimal -28-29 significant digits (128 bit)
        // double gpa = 3.2;
        // bool isMale = true;
        // int characterAge;
        // characterAge = 35;
        // Console.WriteLine("   /|" + characterName.Length);
        // Console.WriteLine("  \"/ |\"" + characterAge + "\ny/o");
        // Console.WriteLine(" /  |");
        // Console.WriteLine("/___|");
        // Console.Write("type something ");
        // string? input = Console.ReadLine();
        // Console.WriteLine("Hey guy " + input);
        // Console.ReadLine();
        // int num = Convert.ToInt32("32");
        // Console.WriteLine(num + 6);
        // SaySomething("me",2);
        // SaySomething("me",3);
        // SaySomething("me",2);
        int [] luckyNumbs = {4,7,15,16,23,24};
        Console.WriteLine(luckyNumbs[1]);
        Console.Write(GetDay(10));
        WeirdWhiles();
        GuessGame()
        Forloop();
        Errorhandling();
*/
    }
    static void GuessGame()
    {
        string secret = "doi";
        string? guess;

        do
        {
            Console.Write("guess something: ");
            guess = Console.ReadLine();
        } while ( guess != secret);

        Console.Write("guessed");
    }
    static void SaySomething(string name, int age)
    {
        Console.WriteLine(name + age);//moves to next line
        Console.Write(name + age);//same line
    }
    static string GetDay(int dayNum)
    {
        string dayName = dayNum switch
        {
            0 => "Sunday",
            1 => "Monday",
            _ => "Invalid",
        };

        // switch (dayNum)
        // {
        //     case 0:
        //         dayName = "Sunday";
        //         break;
        //     case 1:
        //         dayName="Monday";
        //         break;
        //     default:
        //         dayName = "Invalid";
        //         break;
        // }
        return dayName;
    }
    static void WeirdWhiles()
    {
        int index = 6;
        do
        {
            Console.WriteLine(index);
            index++;
        } while (index <= 5);
    }
    static void Forloop()
    {
        for(int i = 1; i<=5; i ++)
        {
            Console.Write(i);
        }
    }
    static void Twodarray()
    {
        int [,] numberGrid = {
            {1,2},
            {3,4},
            {5,6},
        };
    }
    static void Errorhandling()
    {
        try
        {
            Console.Write("Enter a num");
            int num1 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter a num2");
            int num2 = Convert.ToInt32(Console.ReadLine());
            Console.Write(num1/num2);
        }
        catch(DivideByZeroException e)
        {
            Console.WriteLine(e.Message);
        }
        catch(FormatException e)
        {
            Console.WriteLine(e.Message);
        }
        finally
        {
            Console.WriteLine("end");
        }
    }
}
