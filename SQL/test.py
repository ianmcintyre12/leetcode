import re
def check_password(password):
    print(bool(re.search(r'\$\!\@',password)))
    return  (bool(re.search(r'[a-z]+', password))
        and bool(re.search(r'[A-Z]+', password))
        and bool(re.search(r'\d', password))
        and bool(re.search(r'@!',password))
        and 6 <= len(password) <= 12
        )

print(check_password('A0z@ac'))
