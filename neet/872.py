# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def leafSimilar(self, root1: Optional[TreeNode], root2: Optional[TreeNode]) -> bool:
        leafvalue1 = []
        leafvalue2 = []
        def dfs(root,leafvalue):
            if not root:
                return None
            if not root.right and not root.left:
                leafvalue.append(root.val)
                return None
            dfs(root.left,leafvalue)
            dfs(root.right,leafvalue)
        dfs(root1,leafvalue1)
        dfs(root2,leafvalue2)
        return leafvalue1 == leafvalue2
