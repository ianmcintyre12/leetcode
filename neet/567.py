from collections import defaultdict
class Solution:
    def checkInclusion(self, s1: str, s2: str) -> bool:
        counts = defaultdict(int)
        for x in s1:
            counts[x] += 1

        l = 0
        r = 0
        copy = counts.copy()
        while r < len(s2):
            letter = s2[r]
            if copy[letter] > 1:
                # print('1',letter)
                copy[letter] -= 1
                r += 1
            elif copy[letter] == 1:
                # print('2',letter)
                copy[letter] -= 1
                r += 1
                if sum(copy.values()) == 0:
                    return True
            elif copy[letter] == 0:
                # print('3',letter)
                # print(copy)
                copy = counts.copy()
                l += 1
                r = l

        return False

class Solution:
    def checkInclusion(self, s1: str, s2: str) -> bool:
        length = len(s1)
        left = 0
        s1_dict = {}
        for c in s1:
            s1_dict[c] = 1 + s1_dict.get(c, 0)

        s2_dict = {}
        for right in range(len(s2)):
            if s2[right] not in s1_dict:
                s2_dict.clear()
                left = right + 1
                continue

            s2_dict[s2[right]] = 1 + s2_dict.get(s2[right], 0)

            while s2_dict[s2[right]] > s1_dict[s2[right]]:
                s2_dict[s2[left]] -= 1
                left += 1

            if (right - left == length - 1) \
    and (s2_dict[s2[right]] == s1_dict[s2[right]]):
                return True

        return False

s1 ="adc"
s2 ="dcda"

from collections import Counter

def checkInclusion(s1: str, s2: str) -> bool:
    letters = dict(Counter(s1))
    l = 0
    r = len(s1)
    cur = dict(Counter(s2[l:r]))
    if cur == letters:
        return True
    while r < len(s2):
        cur[s2[l]] -= 1
        l += 1
        r += 1
        cur[s2[r - 1]] = 1 + cur.get(s2[r - 1],0)
        cur = {a:b for a,b in cur.items() if b!= 0}
        if cur == letters:
            return True
    return False

print(checkInclusion(s1,s2))
