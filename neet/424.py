class Solution:
    def characterReplacement(self, s: str, k: int) -> int:
        l = 0
        count = {}
        res = 0
        for r in range(len(s)):
            count[s[r]] = 1 + count.get(s[r],0)

            while (r - l + 1) - max(count.values()) > k:
                count[s[l]] -= 1
                l += 1

            res = max(res , r - l + 1)

        return res




x = Solution()
s = "AAAB"
k = 0
print(x.characterReplacement(s,k))

class Solution:
    def characterReplacement(self, s: str, k: int) -> int:
        l = 0
        counts = {}
        longest = 0
        for r in range(len(s)):
            counts[s[r]] = 1 + counts.get(s[r],0)

            while (r - l + 1) - max(counts.values()) > k:
                counts[s[l]] -= 1
                l += 1
            longest = max(longest,r - l +1)
        return longest
