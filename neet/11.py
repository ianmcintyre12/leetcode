# class Solution(object):
#     def maxArea(self, height):
#         """
#         :type height: List[int]
#         :rtype: int
#         """
#         l = 0
#         r = len(height) - 1

#         tot = 0
#         while l < r:
#             cur = (r - l) * min(height[r],height[l])
#             if cur > tot:
#                 tot = cur

#             if height[r] > height[l]:
#                 l += 1
#             else:
#                 r -=1

#         return tot

height = [1,1]

def maxArea(height) -> int:
    result = 0
    l = 0
    r = len(height) - 1
    while l < r:
        tot = min(height[l],height[r]) * (r - l)
        result = max(tot,result)

        if height[r] > height[l]:
            l += 1
        else:
            r -= 1

    return result


print(maxArea(height))
