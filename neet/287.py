class Solution:
    def findDuplicate(self, nums: List[int]) -> int:
        numsdict = {}

        for num in nums:
            if numsdict.get(num):
                return num
            else:
                numsdict[num] = 1


class Solution:
    def findDuplicate(self, nums: List[int]) -> int:
        slow = nums[0]
        fast = nums[0]
        while True:
            slow = nums[slow]
            fast= nums[nums[fast]]
            if slow == fast:    break

        fast = nums[0]
        while slow != fast:
            slow = nums[slow]
            fast = nums[fast]
        return slow
