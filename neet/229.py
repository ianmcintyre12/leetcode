class Solution:
    def majorityElement(self, nums: List[int]) -> List[int]:
        result = []
        for num in nums:
            if num not in result and nums.count(num)>(len(nums)/3):
                result.append(num)
        return result

class Solution:
    def majorityElement(self, nums):
        return [num for num, count in Counter(nums).items() if count > len(nums) // 3]
