class Solution:
    def halvesAreAlike(self, s: str) -> bool:
        vowels = set('aeiou')
        half = int(len(s)/2)

        count_a = 0
        count_b = 0

        for c in range(half):
            if s[c].lower() in vowels:
                count_a += 1
            if s[c + half].lower() in vowels:
                count_b += 1
        return count_a == count_b
