# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def isSubtree(self, root: Optional[TreeNode], subRoot: Optional[TreeNode]) -> bool:
        sub = []
        second = []
        check = [False]

        def dfs(root,array):
            if root is None:
                array.append('None')
                return None

            left = dfs(root.left,array)
            right = dfs(root.right,array)

            array.append(root.val)
            return root
        def check_main(root,array,subroot,sub):
            if root is None:
                return None
            if root.val == subroot.val:
                dfs(root,second)
                if sub == array:
                    check[0] = True
                second.clear()

            check_main(root.left,array,subroot,sub)
            check_main(root.right,array,subroot,sub)

            return root
        dfs(subRoot,sub)
        check_main(root,second,subRoot,sub)
        return check[0]

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def isSubtree(self, root: Optional[TreeNode], subRoot: Optional[TreeNode]) -> bool:
        if not subRoot: return True
        if not root: return False
        if self.sameTree(root,subRoot):
            return True

        return (self.isSubtree(root.left,subRoot) or self.isSubtree(root.right,subRoot))

    def sameTree(self,s,t):
        if not s and not t:
            return True

        if s and t and s.val == t.val:
            return (self.sameTree(s.left,t.left) and self.sameTree(s.right,t.right))

        return False
