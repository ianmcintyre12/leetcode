class Solution:
    def findWinners(self, matches: List[List[int]]) -> List[List[int]]:
        lost = {}
        for match in matches:
            lost[match[0]] = lost.get(match[0],0)
            lost[match[1]] = lost.get(match[1],0) + 1
        result = [[],[]]
        for p,l in lost.items():
            if l == 0:
                result[0].append(p)
            elif l ==1:
                result[1].append(p)
        result[0].sort()
        result[1].sort()
        return result
