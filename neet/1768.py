class Solution:
    def mergeAlternately(self, word1: str, word2: str) -> str:
        result = ''
        p = 0
        remain,short = word1, word2
        if len(word2) > len(word1):
            remain,short = word2,word1
        while p < len(short):
            result += word1[p]
            result += word2[p]
            p += 1
        return result + remain[p:]

class Solution:
    def mergeAlternately(self, word1: str, word2: str) -> str:
        result = ''
        p = 0

        while p < min(len(word1),len(word2)):
            result += word1[p]
            result += word2[p]
            p += 1
        result += word1[p:]
        result += word2[p:]
        return result
