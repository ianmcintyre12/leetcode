class Solution:
    def climbStairs(self, n: int) -> int:
        if n == 1:
            return 1
        l = 1
        r = 1
        t = 0
        for i in range(n-1):
            t = l + r
            r = r + l
            l = r - l
        return t
