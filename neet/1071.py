class Solution:
    def gcdOfStrings(self, str1: str, str2: str) -> str:
        result = ''
        leng = sorted([str1,str2], key=len)
        cur = ''
        for c in leng[0]:
            long = leng[1]
            short = leng[0]
            cur += c
            long = long.replace(cur,'')
            short = short.replace(cur,'')
            if not long and not short:
                result = cur
        return result

str1 = 'ABABAB'
str2 ='ABAB'
solution = Solution()
print(solution.gcdOfStrings(str1,str2)
)
