# class Solution(object):
#     def maxProfit(self, prices):
#         """
#         :type prices: List[int]
#         :rtype: int
#         """
#         l = 0
#         r = 1
#         max_profit = 0
#         while r < len(prices):
#             profit = prices[r] - prices[l]
#             if profit > max_profit:
#                 max_profit = profit
#             if (prices[l] > prices[r]):
#                 l = r
#             r += 1

#         return max_profit


nums = [7,1,5,3,6,4]
class Solution:
    def maxProfit(prices) -> int:
        profit = 0
        buy = 0
        sell = 1
        while sell < len(prices):
            potential = prices[sell] - prices[buy]
            profit = max(potential,profit)

            if prices[buy] > prices[sell]:
                buy = sell
            sell += 1
        return profit

print(Solution.maxProfit(nums))
