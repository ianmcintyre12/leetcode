# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def removeNthFromEnd(self, head: Optional[ListNode], n: int) -> Optional[ListNode]:
        #start with 1 pass to see length
        counter = 0
        cur = head
        while cur:
            cur = cur.next
            counter += 1

        if counter == 1:
            return None
        elif counter == n:
            return head.next
        #connect prev to post
        prev = None
        cur = head
        for i in range(counter - n):
            prev = cur
            cur = cur.next
        skip = None
        if cur.next:
            skip = cur.next
        prev.next = cur.next

        return head


# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def removeNthFromEnd(self, head: Optional[ListNode], n: int) -> Optional[ListNode]:

        dummy = ListNode(0,head)
        left = dummy
        right = head

        while n > 0 and right:
            right = right.next
            n -= 1

        while right:
            left = left.next
            right = right.next

        left.next = left.next.next

        return dummy.next
