# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def reverseBetween(self, head: Optional[ListNode], left: int, right: int) -> Optional[ListNode]:
        dummy = ListNode(0,head)

        leftPrev,cur = dummy, head
        for i in range(left - 1):
            leftPrev, cur = cur,cur.next

        prev = None
        for i in range(right-left + 1):
            tmpNext= cur.next
            cur.next = prev
            prev,cur = cur,tmpNext

        leftPrev.next.next = cur
        leftPrev.next = prev
        return dummy.next


# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def reverseBetween(self, head: Optional[ListNode], left: int, right: int) -> Optional[ListNode]:
        prev = None
        pos = 1
        tmp1 = None
        leftpre = None
        leftpost = None
        cur = head
        while pos < right:
            if pos == left:
                tmp1 = cur
                leftpre = prev
                leftpost = cur.next

            prev = cur
            cur = cur.next
            pos += 1
        if leftpre and leftpost and tmp1:
            rightpost = cur.next
            cur.next = leftpost
            leftpre.next = cur
            if prev:
                prev.next = tmp1
            tmp1.next = rightpost

        return head
