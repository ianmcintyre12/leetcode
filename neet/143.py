# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution(object):
    def reorderList(self, head):
        """
        :type head: ListNode
        :rtype: None Do not return anything, modify head in-place instead.
        """
        slow = head
        fast = head.next

        while fast and fast.next:
            slow = slow.next
            fast = fast.next.next

        second = slow.next
        slow.next = None

        rev = None

        while second != None:
            tmp = second.next # 4 None
            second.next = rev # 3-> 3
            rev = second #3 4
            second = tmp # 4 None

        cur = head
        cur2 = rev
        while cur2 != None:
            tmp1 = cur.next
            tmp2 = cur2.next
            cur.next = cur2
            cur2.next = tmp1
            cur = tmp1
            cur2 = tmp2


# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def reorderList(self, head: Optional[ListNode]) -> None:
        """
        Do not return anything, modify head in-place instead.
        """
        slow = head
        fast = head.next
        while fast and fast.next:
            slow = slow.next
            fast = fast.next.next

        second = slow.next
        slow.next = None
        #reverse second

        prev = None
        cur = second
        while cur:
            tmp = cur.next
            cur.next = prev
            prev = cur
            cur = tmp
        print(head)
        print(prev)
        cur1 = head
        cur2 = prev
        while cur2:
            tmp1 = cur1.next
            tmp2 = cur2.next
            cur2.next = cur1.next
            cur1.next = cur2
            cur2 = tmp2
            cur1 = tmp1


# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def reorderList(self, head: Optional[ListNode]) -> None:
        """
        Do not return anything, modify head in-place instead.
        """
        #split list in half
        slow = head
        fast = head.next

        while fast and fast.next:
            slow = slow.next
            fast = fast.next.next

        second = slow.next
        slow.next = None

        prev = None
        while second:
            tmp = second.next
            second.next = prev
            prev = second
            second = tmp

        cur = head
        while prev:
            tmp1 = cur.next
            tmp2 =prev.next
            prev.next=cur.next
            cur.next=prev
            cur=tmp1
            prev=tmp2

        return head
