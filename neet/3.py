class Solution(object):
    def lengthOfLongestSubstring(self, s):
        """
        :type s: str
        :rtype: int
        """
        if s == '':
            return 0
        result = 1
        sub = s[0]
        l = 0
        r = 1
        while r < len(s):
            if s[r] not in sub:
                sub += s[r]
                r += 1
                if len(sub) > result:
                    result = len(sub)
            else:
                l += 1
                r = l + 1
                sub = s[l]

        return result

s = "abcabcbb"
# def lengthOfLongestSubstring(s: str) -> int:
#     if len(s) == 0:
#         return 0
#     unique = {}
#     l,r = 0,0
#     longest = 1

#     while r < len(s):
#         if not unique.get(s[r]):
#             unique[s[r]] = True
#             r += 1
#         else:
#             l += 1
#             r = l + 1
#             unique = {
#                 s[l]: True
#             }
#         longest = max(longest, r - l)
#     return longest
def lengthOfLongestSubstring(s: str) -> int:
    charSet = set()
    l = 0
    longest = 0
    for r in range(len(s)):
        while s[r] in charSet:
            charSet.remove(s[l])
            l += 1
        charSet.add(s[r])
        longest = max(longest, r - l + 1)
    return longest

print(lengthOfLongestSubstring(s))
