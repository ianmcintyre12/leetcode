# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
# class Solution:
#     def addTwoNumbers(self, l1: Optional[ListNode], l2: Optional[ListNode]) -> Optional[ListNode]:
#         dummy = ListNode(0)
#         cur1 = l1
#         cur2 = l2
#         cur3 = dummy
#         carry = 0
#         while cur1 or cur2:
#             val1 = 0
#             val2 = 0
#             if cur1:
#                 val1 = cur1.val
#             if cur2:
#                 val2 = cur2.val
#             val = val1 + val2
#             if carry > 0:
#                 val += 1
#                 carry -= 1
#             if val > 9:
#                 carry += 1
#                 val -= 10

#             new = ListNode(val)
#             cur3.next = new
#             if cur1:
#                 cur1 = cur1.next
#             if cur2:
#                 cur2 = cur2.next
#             cur3 = cur3.next

#         if carry > 0:
#             new = ListNode(carry)
#             cur3.next = new
#         return dummy.next

print('asdf')
