class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        prefix = strs[0]

        pointer = 0
        for words in strs:
            pre = ''
            while pointer <= len(prefix) - 1 and pointer <= len(words) - 1 and prefix[pointer] == words[pointer]:
                pre += prefix[pointer]
                pointer += 1
            prefix = pre
            pointer = 0
        return prefix
