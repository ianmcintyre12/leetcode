/**
 * @param {number[]} arr
 * @param {Function} fn
 * @return {number[]}
 */
var map = function(arr, fn) {
    let result = []
    arr.forEach((n,index) => result.push(fn(n,index)))
    return result
};
