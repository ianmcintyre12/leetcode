import random
class RandomizedSet:

    def __init__(self):
        self.dictidx = {}
        self.items = []

    def insert(self, val: int) -> bool:
        if val in self.dictidx:
            return False

        self.items.append(val)
        self.dictidx[val] = len(self.items) - 1
        return True

    def remove(self, val: int) -> bool:
        if val in self.dictidx:
            if len(self.items) == 1:
                self.items = []
                self.dictidx = {}
                return True
            idx = self.dictidx[val]
            self.items[idx] = self.items[-1]
            self.dictidx[self.items[-1]] = idx
            self.items.pop()
            del self.dictidx[val]
            return True
        return False

    def getRandom(self) -> int:
        return random.choice(self.items)
